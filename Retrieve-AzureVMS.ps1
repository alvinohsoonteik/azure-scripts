<#

    .SYNOPSIS 
        Retrieve all the Azure VMs in a specific Azure Resource Group

    .ARGS
        Argument [0] - Azure Account Service Username
        Argument [1] - Azure Account Service Password
        Argument [2] - Azure Subscription
        Example: Retrieve-AzureVMS.ps1 '<username>' '<password>' '<subscription>'

    .DEPENDENCY
        Dependency installation that needs to happen before this script can be executed:
        - Installation of Windows Management Framework (WMF 5.0) - https://msdn.microsoft.com/en-us/powershell/wmf/5.0/requirements
        - Installation of Azure Resource Manager Cmdlets through PowerShell Terminal (Install-Module AzureRM)

    .DESCRIPTION
        This sample runbooks retrieve all of the virtual machines in the specified Azure Resource Group within Azure Subscription.
    
    .NOTES
        AUTHOR: Alvin Oh, Vibrato
        LASTEDIT: April 18, 2017
#>

Import-Module AzureRM

#Arguments
$username = $args[0]
$password = $args[1]
$subscription = $args[2]

#Get the credential login for Azure environment
$azureUsername = "$username"
$azurePassword = ConvertTo-SecureString "$password" -AsPlainText -Force
$AllResourceGroups = @()

#Connect to your Azure RM Account
if($azureUsername -And $azurePassword) {
    #Login using the credential
    $psCred = New-Object -typename System.Management.Automation.PSCredential($azureUsername, $azurePassword)
    Add-AzureRmAccount -Credential $psCred
} else {
    Throw "Could not authenticate to Azure using current credential. Make sure the user name and password are correct."
}

#Retrieving Azure Subscription
$Subscriptions = "$subscription"

if(!$Subscriptions) {
    Throw "Subscription could not be determine. Make sure the subscription argument is passed in correctly."
}

Write-Host "Selecting Subscription: $($Subscriptions)"
Select-AzureRmSubscription -SubscriptionName $Subscriptions | Out-Null

# Cloud Service End-Point - Required WINRM
#$ResourceRm = Get-AzureRmResource | Select-Object ResourceGroupName, ResourceType, ResourceName
#foreach ($RRM in $ResourceRm)
#{
#    (Get-AzureRmResource -ResourceGroupName $RRM.ResourceGroupName -ResourceType $RRM.ResourceType -ResourceName $RRM.ResourceName).Properties.hostname
#}
    
#Retrieving all Virtual Machines
$vms = (Get-AzureRmNetworkInterface | Get-AzureRmNetworkInterfaceIpConfig | Select-Object PrivateIpAddress).PrivateIpAddress   
foreach ($vm in $vms)
{
    #Execution of Chef InSpec (Windows Baseline)
    Write-Host "Running Chef InSpec - Windows Baseline At $vm"
    inspec exec https://github.com/dev-sec/windows-baseline -t winrm://$azureUsername@$vm --password $azurePassword | tee-object C:\opscode\windows-baseline-$vm.txt
        
    #Execution of Chef InSpec (Windows Patch Baseline)
    Write-Host "Running Chef InSpec - Windows Patch Baseline At $vm"
    inspec exec https://github.com/dev-sec/windows-patch-baseline -t winrm://$azureUsername@$vm --password $azurePassword | tee-object C:\opscode\patch-baseline-$vm.txt
        
    #Execution of Chef InSpec (SSL Baseline)
    Write-Host "Running Chef InSpec - SSL Baseline At $vm"
    inspec exec https://github.com/dev-sec/ssl-baseline -t winrm://'$azureUsername'@$vm --password $azurePassword | tee-object C:\opscode\ssl-baseline-$vm.txt
}

#Manual Execution of Chef InSpec on Selective Server
#inspec exec https://github.com/dev-sec/windows-baseline -t winrm://'ptadmin'@23.101.18.150 --password '' --ssl --self-signed --format json | tee-object C:\opscode\windows-baseline-pttardistest.json
#inspec exec https://github.com/dev-sec/windows-patch-baseline -t winrm://'ptadmin'@23.101.18.150 --password '' --ssl --self-signed --format json  | tee-object C:\opscode\patch-baseline-pttardistest.json
#inspec exec https://github.com/dev-sec/ssl-baseline -t winrm://'ptadmin'@23.101.18.150 --password '' --ssl --self-signed --format json | tee-object C:\opscode\ssl-baseline-pttardistest.json