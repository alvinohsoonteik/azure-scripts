﻿<#

    .SYNOPSIS 
        Retrieve all the Azure VMs in a specific Azure Resource Group

    .ARGS
        Argument [0] - Azure Account Service Username
        Argument [1] - Azure Account Service Password
        Argument [2] - Azure Subscription
        Argument [3] - Window Box Password
        Example: Retrieve-AzureVMS.ps1 '<username>' '<password>' '<subscription>' '<win_password>'

    .DEPENDENCY
        Dependency installation that needs to happen before this script can be executed:
        - Installation of Windows Management Framework (WMF 5.0) - https://msdn.microsoft.com/en-us/powershell/wmf/5.0/requirements
        - Installation of Azure Resource Manager Cmdlets through PowerShell Terminal (Install-Module AzureRM)

    .DESCRIPTION
        This sample runbooks retrieve all of the virtual machines in the specified Azure Resource Group within Azure Subscription.
    
    .NOTES
        AUTHOR: Alvin Oh, Vibrato
        LASTEDIT: April 18, 2017
#>

param(
    [Parameter(Position=0,mandatory=$true)]
    [string] $username,
    [Parameter(Position=1,mandatory=$true)]
    [string] $password,
    [Parameter(Position=2,mandatory=$true)]
    [string] $subscription,
    [string] $win_username = "ptadmin",
    [Parameter(Position=3,mandatory=$true)]
    [string] $win_password
)

Import-Module AzureRM

#Get the credential login for Azure environment
$azureUsername = "$username"
$azurePassword = ConvertTo-SecureString "$password" -AsPlainText -Force
$AllResourceGroups = @()

#Connect to your Azure RM Account
if($azureUsername -And $azurePassword) {
    #Login using the credential
    $psCred = New-Object -typename System.Management.Automation.PSCredential($azureUsername, $azurePassword)
    Add-AzureRmAccount -Credential $psCred
} else {
    Throw "Could not authenticate to Azure using current credential. Make sure the user name and password are correct."
}

#Retrieving Azure Subscription
$Subscriptions = "$subscription"

if(!$Subscriptions) {
    Throw "Subscription could not be determine. Make sure the subscription argument is passed in correctly."
}

Write-Host "Selecting Subscription: $($Subscriptions)"
Select-AzureRmSubscription -SubscriptionName $Subscriptions | Out-Null

#Retrieving Resources
$Resources += Get-AzureRmResource

$VMs = $Resources.Where({$_.ResourceType.Equals('Microsoft.ClassicCompute/virtualMachines')})
$VMResources = @{}

foreach ($VM in $VMs)
{    
    Write-Host $VM.Name -ForegroundColor Cyan    
    $VMResources.Add($VM.Name, (Get-AzureRmResource -ResourceId $VM.ResourceId))    
    if ($VMResources[$VM.Name].Properties.instanceView.powerState -eq 'Started')    
    {        
        $Endpoint = $VMResources[$VM.Name].Properties.networkProfile.inputEndpoints.Where({$_.privatePort -eq 5986})
        if ($Endpoint[0].privatePort -eq 5986)
        {        
            Write-Host "Running Chef InSpec winrm://$($Endpoint[0].publicIpAddress):$($Endpoint[0].publicPort) --ssl --self-signed"
            inspec exec https://github.com/dev-sec/windows-baseline -t winrm://$win_username@$($Endpoint[0].publicIpAddress):$($Endpoint[0].publicPort) --password $win_password --ssl --self-signed --format json | tee-object C:\opscode\windows-baseline-$($VM.Name).json
            inspec exec https://github.com/dev-sec/windows-patch-baseline -t winrm://$win_username@$($Endpoint[0].publicIpAddress):$($Endpoint[0].publicPort) --password $win_password --ssl --self-signed --format json | tee-object C:\opscode\patch-baseline-$($VM.Name).json
            inspec exec https://github.com/dev-sec/ssl-baseline -t winrm://$win_username@$($Endpoint[0].publicIpAddress):$($Endpoint[0].publicPort) --password $win_password --ssl --self-signed --format json | tee-object C:\opscode\ssl-baseline-$($VM.Name).json
        }
        else
        {
            Write-Host "- No WINRM Public Endpoint"
        }    
    }
    else
    {
        Write-Host "- Virtual Machine in SHUTDOWN mode"
    } 
}